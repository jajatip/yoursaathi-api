const mongoose = require("../db");
const schema = new mongoose.Schema({
    name : String,
    message : String,
},{
        strict: true,
        versionKey: false,
        timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
    }
);

module.exports = mongoose.model('Message', schema)