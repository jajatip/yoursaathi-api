const roles = require('../../helpers/role');
function adminAuth(req, res, next) {
    if (req.curentUser && req.curentUser.roles == roles.User && req.curentUser._id != req.params.id) {
        return res.status(403).send({ auth: false, message: 'Access denied!.' });
    }
    if (req.curentUser && req.curentUser.roles == roles.User && req.curentUser._id == req.params.id && (req.method =='POST' || req.method =='DELETE')) {
        return res.status(403).send({ auth: false, message: 'Access denied!.' });
    }

    next();
}

module.exports = adminAuth;