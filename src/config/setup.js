const users = require("../models/user-model");
const message = require("../models/message-model");
const bcrypt = require('bcryptjs');
var hashedPassword = bcrypt.hashSync('test123', 8);
const mongoose = require("../db");
const ObjectId = mongoose.Types.ObjectId;
const user1 = {
    _id: ObjectId().toString(),
    email: "jajatipanda92@gmail.com",
    name: "Jajati Panda",
    age: 27,
    gender: 'Male',
    userType: 'Admin',
    password: hashedPassword,
    details: {
    	address1: 'N-105',
    	address2: 'Sector-12'
    }
};
const user2 = {
    _id: ObjectId().toString(),
    email: "Anil@gmail.com",
    name: "Anil",
    age: 23,
    gender: 'Male',
    userType: 'User',
    password: hashedPassword,
    details: {
      address1: 'N-106',
      address2: 'Sector-12'
    }
};

const msg1 = {
  _id: ObjectId().toString(),
  message : 'msg one',
  name: "bapi",
};

const msg2 = {
  _id: ObjectId().toString(),
  message : 'msg two',
  name: "pintu",
};

const dropTestDbs = () => {
  return Promise.all([
    users.remove({}),
    message.remove({}),
  ]);
};

dropTestDbs()
  .then(() => Promise.all([
    users.create(user1),
    users.create(user2), 
    message.create(msg1), 
    message.create(msg2), 
  ]))
  .then(() => {
    console.log(`users, user1 _id: ${user1._id} and user2 _id: ${user2._id}`);
    console.log(`message, msg1 _id: ${msg1._id} and msg2 _id: ${msg2._id}`);
  })
  .catch((err) => {
   console.log(err);
  });