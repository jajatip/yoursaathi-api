module.exports = {
  mongoConfig: {
    url: 'mongodb://127.0.0.1:27017/yoursaathi'
  },
  port: 3000,
  secret: 'supersecret',
  expiresIn: 86400, // expires in 24 hours,
  isConsoleLog: true,
  isTestSetup: true	
};