const path = require('path');
const config = require("./config");
let app = require('express')();
let server = require('http').createServer(app);
let io = require('socket.io')(server);
const port = process.env.PORT || config.port;
const isTestSetup = config.isTestSetup;
const cors = require('cors');
app.use(cors({
  'allowedHeaders': ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'x-access-token'],
  'exposedHeaders': ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'x-access-token'],
  'origin': 'http://localhost:8101',
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
  'preflightContinue': false
}));

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", 'http://localhost:8101');
//   res.header("Access-Control-Allow-Credentials", true);
//   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
//   res.header("Access-Control-Allow-Headers",
// 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json,Authorization');
//   next();
// });

const bodyParser = require('body-parser');

const auth = require('./routes/auth/verify-token');



if(isTestSetup) {
	require('./config/setup');
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/auth', require('./routes/auth/index'));
app.use('/users', auth, require('./routes/user-routes'));
app.use('/signup', require('./routes/register.routes'));





io.on('connection', (socket) => {
 
  socket.on('disconnect', function(){
    io.emit('users-changed', {user: socket.username, event: 'left'});   
  });
 
  socket.on('set-name', (name) => {
    socket.username = name;
    io.emit('users-changed', {user: name, event: 'joined'});    
  });
  
  socket.on('send-message', (message) => {
    io.emit('message', {msg: message.text, user: socket.username, createdAt: new Date()});    
  });
});
 
server.listen(port, function(){
   console.log('listening in http://localhost:' + port);
});

