FROM node:7.5.0

WORKDIR /app

# Install app dependencies
COPY ./package.json /app/package.json
RUN npm install --production
RUN npm install --global pm2

# Copy the app sources
COPY ./lib /app/lib

# Expose Ports
EXPOSE  8080

CMD [ "npm", "start" ]
